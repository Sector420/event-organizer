import 'package:costaccountant_app/database/messages_ds.dart';
import 'package:get_it/get_it.dart';

import '../bloc/chat/chat_bloc.dart';
import '../bloc/costs/costs_bloc.dart';
import '../bloc/events/events_bloc.dart';
import '../bloc/profile/profile_bloc.dart';
import '../database/costs_ds.dart';
import '../database/events_ds.dart';
import '../database/fb_service.dart';
import '../database/fb_api.dart';
import '../database/profiles_ds.dart';
import '../domain/interactor/interactor.dart';


final injector = GetIt.instance;

void initDependencies() {
    injector.registerSingleton<FBApi>(FBService());

    injector.registerSingleton(EventsDataSource(injector<FBApi>()),);
    injector.registerSingleton(CostsDataSource(injector<FBApi>()),);
    injector.registerSingleton(MessagesDataSource(injector<FBApi>()),);
    injector.registerSingleton(ProfilesDataSource(injector<FBApi>()),);

    injector.registerSingletonAsync(
            () async {
          return Interactor(
            injector<EventsDataSource>(),
            injector<CostsDataSource>(),
            injector<MessagesDataSource>(),
            injector<ProfilesDataSource>(),
          );
        }
    );

    injector.registerFactory(
          () => EventsBloc(
        injector<Interactor>(),
      ),
    );

    injector.registerFactory(
          () => CostsBloc(
        injector<Interactor>(),
      ),
    );

    injector.registerFactory(
          () => ChatBloc(
        injector<Interactor>(),
      ),
    );

    injector.registerFactory(
          () => ProfileBloc(
        injector<Interactor>(),
      ),
    );

}