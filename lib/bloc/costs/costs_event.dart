part of 'costs_bloc.dart';

@immutable
abstract class CostsEvent {}

class InitialCosts extends CostsEvent {
  final String eventId;

  InitialCosts(this.eventId);
}

class UploadCost extends CostsEvent {
  final String eventId;
  final Cost newCost;

  UploadCost(this.eventId, this.newCost);
}

class DeleteCost extends CostsEvent {
  final String eventId;
  final String costId;
  final Cost cost;

  DeleteCost(this.eventId, this.costId, this.cost);
}

class Waiting extends CostsEvent {

  Waiting();
}

