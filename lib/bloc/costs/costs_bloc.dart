import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:costaccountant_app/domain/model/cost.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../domain/interactor/interactor.dart';

part 'costs_event.dart';
part 'costs_state.dart';

class CostsBloc extends Bloc<CostsEvent, CostsState> {
  final Interactor _interactor;

  CostsBloc(this._interactor) : super(InitialState()) {
    on<InitialCosts>((event, emit) async  {
      var profiles = await _interactor.getProfiles();
      emit(LoadedState(_interactor.getCosts(event.eventId), profiles));
    });
    on<UploadCost>((event, emit) async {
      _interactor.addCost(event.eventId, event.newCost);
      var cevenet = await _interactor.getEventById(event.eventId);
      var cost = (int.parse(cevenet.body!) + int.parse(event.newCost.body)).toString();
      _interactor.addCostToEvent(cost, event.eventId);
      print("fasz");
      emit(Changed(cost));
    });
    on<DeleteCost>((event, emit) async {
      _interactor.deleteCost(event.eventId, event.costId);
      var cevenet = await _interactor.getEventById(event.eventId);
      var cost = (int.parse(cevenet.body!) - int.parse(event.cost.body)).toString();
      _interactor.addCostToEvent(cost, event.eventId);
      print("töröltem");
      emit(Changed(cost));
    });
    on<Waiting>((event, emit) {
      print("waiting");
      emit(WaitingState());
    });
  }
}
