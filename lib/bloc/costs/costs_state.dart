part of 'costs_bloc.dart';

@immutable
abstract class CostsState extends Equatable{}

class InitialState extends CostsState {

  @override
  List<Object?> get props => [];
}

class LoadedState extends CostsState {
  final Stream<QuerySnapshot<Map<String, dynamic>>> costs;
  final profiles;

  LoadedState(this.costs,this.profiles);

  @override
  List<Object?> get props => [];
}

class Changed extends CostsState {
  String cost;

  Changed(this.cost);

  @override
  List<Object?> get props => [];
}

class WaitingState extends CostsState {


  @override
  List<Object?> get props => [];
}

