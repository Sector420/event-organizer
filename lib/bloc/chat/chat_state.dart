part of 'chat_bloc.dart';

@immutable
abstract class ChatState extends Equatable{}

class InitialState extends ChatState {

  @override
  List<Object?> get props => [];
}

class LoadedState extends ChatState {
  final Stream<QuerySnapshot<Map<String, dynamic>>> messages;

  LoadedState(this.messages);

  @override
  List<Object?> get props => [];
}

class FileSelected extends ChatState {
  FilePickerResult? result;

  FileSelected(this.result);

  @override
  List<Object?> get props => [];
}
