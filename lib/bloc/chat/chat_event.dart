part of 'chat_bloc.dart';

@immutable
abstract class ChatEvent {}

class InitialChat extends ChatEvent {
  final String eventId;
  final String costId;

  InitialChat(this.eventId, this.costId);
}

class PickedFileEvent extends ChatEvent{

}

class UploadMessage extends ChatEvent {
  final String eventId;
  final String costId;
  final Map<String, dynamic> newMessage;

  UploadMessage(this.eventId, this.costId, this.newMessage);
}
