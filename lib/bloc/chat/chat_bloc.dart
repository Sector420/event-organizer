import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:file_picker/file_picker.dart';
import 'package:meta/meta.dart';

import '../../dialogs/show_picture_dialog.dart';
import '../../domain/interactor/interactor.dart';

part 'chat_event.dart';
part 'chat_state.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  final Interactor _interactor;
  late String eventId;
  late String costId;

  ChatBloc(this._interactor) : super(InitialState()) {
    on<InitialChat>((event, emit) async  {
      eventId = event.eventId;
      costId = event.costId;
      emit(LoadedState(_interactor.getMessages(event.eventId, event.costId)));
    });
    on<UploadMessage>((event, emit) async {
      _interactor.addMessage(event.eventId, event.costId, event.newMessage);
    });
    on<PickedFileEvent>((event, emit) async {
      final result = await FilePicker.platform.pickFiles();
      if (result == null) {
        emit(LoadedState(_interactor.getMessages(eventId, costId)));
      }
      else {
        emit(FileSelected(result));
      }
    });
  }
}
