part of 'profile_bloc.dart';

@immutable
abstract class ProfileState extends Equatable{}

class ProfileInitialState extends ProfileState {

  @override
  List<Object?> get props => [];
}

class ProfileLoadedState extends ProfileState {
  final Profile? profile;

  ProfileLoadedState(this.profile);

  @override
  List<Object?> get props => [profile];
}

class ProfileFileSelected extends ProfileState {
  FilePickerResult? result;

  ProfileFileSelected(this.result);

  @override
  List<Object?> get props => [result];
}

class ProfileErrorEventState extends ProfileState {
  final String message;

  ProfileErrorEventState(this.message);

  @override
  List<Object?> get props => [message];
}



