part of 'events_bloc.dart';

@immutable
abstract class EventsState extends Equatable{}


class StartingState extends EventsState {
  final Stream<QuerySnapshot<Map<String, dynamic>>> events;

  StartingState(this.events);

  @override
  List<Object?> get props => [events];
}

class InitialState extends EventsState {
  final Stream<QuerySnapshot<Map<String, dynamic>>> events;
  final profiles;

  InitialState(this.events, this.profiles);

  @override
  List<Object?> get props => [events];
}

class SearchResultState extends EventsState {
  final Stream<QuerySnapshot<Map<String, dynamic>>> events;

  SearchResultState(this.events);

  @override
  List<Object?> get props => [events];
}

class SearchErrorEventState extends EventsState {
  final String message;

  SearchErrorEventState(this.message);

  @override
  List<Object?> get props => [message];
}
