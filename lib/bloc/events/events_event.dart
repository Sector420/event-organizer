part of 'events_bloc.dart';

@immutable
abstract class EventsEvent {}

class LoadProfileEvent extends EventsEvent{
  User user;

  LoadProfileEvent(this.user);
}

class UpdateSearchEvent extends EventsEvent {
  final String text;

  UpdateSearchEvent(this.text);
}

class UploadEvent extends EventsEvent {
  final  Map<String, dynamic> newEvent;

  UploadEvent(this.newEvent);
}

class DeleteEvent extends EventsEvent {
  final String id;

  DeleteEvent(this.id);
}