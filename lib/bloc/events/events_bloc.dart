import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';

import '../../domain/interactor/interactor.dart';

part 'events_event.dart';
part 'events_state.dart';

class EventsBloc extends Bloc<EventsEvent, EventsState> {
  final Interactor _interactor;
  late var email;

  EventsBloc(this._interactor) : super(StartingState(_interactor.kapdbeafaszom())) {
    on<LoadProfileEvent>((event, emit) async {
      var profiles = await _interactor.getProfiles();
      email = event.user.email!;
      emit(InitialState(_interactor.getEvents(email), profiles));
    }
    );
    on<UpdateSearchEvent>((event, emit) async {
      var profiles = await _interactor.getProfiles();
      try {
        var text = event.text;
        if(text.isEmpty) {
          emit(InitialState(_interactor.getEvents(email), profiles));
        }
        else {
          var searchResult = _interactor.getSearchResult(event.text);
          emit(SearchResultState(searchResult));
        }
      } catch (e) {
        emit(SearchErrorEventState(e.toString()));
      }
    },
    );
    on<UploadEvent>((event, emit) async {
      _interactor.addEvent(event.newEvent);
    },
    );
    on<DeleteEvent>((event, emit) async {
      _interactor.deleteEvent(event.id);
    },
    );
  }
}
