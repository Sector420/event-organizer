import 'dart:io';

import 'package:costaccountant_app/bloc/profile/profile_bloc.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../di/di_utils.dart';

class ProfilePage extends StatelessWidget {
  final user = FirebaseAuth.instance.currentUser;

  PlatformFile? pickedFile;

  late String id;

  String jake = "https://firebasestorage.googleapis.com/v0/b/flutterca-9f19c.appspot.com/o/test%2FJakeGyllenhaal.jpg?alt=media&token=96811b4f-6849-4fb1-a43a-5eeeb399c619";

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => injector<ProfileBloc>()..add(LoadProfileEvent(user!.email!)),
      child: MaterialApp(
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: Scaffold(
            appBar: AppBar(title: const Text("Valami")),
            body: Align(
                  alignment: Alignment.topRight,
                  child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(20),
                              child: IntrinsicHeight(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    BlocBuilder<ProfileBloc, ProfileState>(
                                      builder: (context, state) {
                                          var url = jake;
                                          if (state is ProfileLoadedState) {
                                            final profile = state.profile;
                                            profile == null ? url = jake : url = profile.imageUrl!;
                                            return CircleAvatar(
                                              radius: 80,
                                              backgroundImage: NetworkImage(url),
                                            );
                                          }
                                          if(state is ProfileFileSelected) {
                                            pickedFile = state.result!.files.first;
                                            return CircleAvatar(
                                              radius: 80,
                                              backgroundImage: FileImage(File(pickedFile!.path!)),
                                            );
                                          }
                                          else {
                                            return CircularProgressIndicator();
                                          }
                                        }
                                    ),
                                    Builder(
                                      builder: (context) {
                                        return Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            ElevatedButton(
                                              child: const Text("Select File"),
                                              onPressed: () {
                                                context.read<ProfileBloc>().add(PickedFileEvent());
                                              },
                                            ),
                                            ElevatedButton(
                                              child: const Text("Upload File"),
                                              onPressed: () {
                                                if(pickedFile != null) {
                                                  context.read<ProfileBloc>().add(UploadFileEvent(pickedFile));
                                                }
                                                else {
                                                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("You didn't chose any file")));
                                                }
                                              },
                                            ),
                                          ],
                                        );
                                      }
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            BlocBuilder<ProfileBloc, ProfileState>(
                                builder: (context, state) {
                                  if(state is ProfileLoadedState) {
                                    final profile = state.profile;
                                    var title = "asd";
                                    profile == null ? title= "No Profile" : title = profile.username;
                                    return Column(
                                      children: [
                                        Text(
                                            title,
                                            style: const TextStyle(fontSize: 30)
                                        ),
                                        const SizedBox(height: 15),
                                        Text(
                                            profile!.email,
                                            style: const TextStyle(fontSize: 15)
                                        ),
                                      ],
                                    );
                                  }
                                  else {
                                    return const CircularProgressIndicator();
                                  }
                                }
                            ),
                          ],
                        ),
                ),
          ),
        ),
      );
  }
}