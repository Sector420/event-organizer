import 'package:costaccountant_app/domain/model/message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../items/photo_item.dart';


class PicturesPage extends StatelessWidget {
  List<String> messages;

  PicturesPage(this.messages);

  @override
  Widget build(BuildContext context) {
    //final arguments = ModalRoute.of(context)!.settings.arguments as Map<String, List<String>>;
    //List<String> messages = arguments["messages"] as List<String>;
    return Scaffold(
        appBar: AppBar(
          title: const Text("Pictures"),
      ),
      body: GridView.builder(
        itemCount: messages.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          mainAxisExtent: 100,
          crossAxisCount: 3,
          mainAxisSpacing: 4,
          crossAxisSpacing: 4,
          childAspectRatio: 1 / 1,
        ),
        itemBuilder: (context, index) {
          var item = PhotoItem(index, messages);
          return item;
        },
      ),
    );
  }

}