import 'dart:io';

import 'package:costaccountant_app/bloc/chat/chat_bloc.dart';
import 'package:costaccountant_app/di/di_utils.dart';
import 'package:costaccountant_app/dialogs/show_picture_dialog.dart';
import 'package:costaccountant_app/domain/interactor/interactor.dart';
import 'package:costaccountant_app/domain/model/message.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MessageBox extends StatelessWidget {
  final user = FirebaseAuth.instance.currentUser;
  String eventId;
  String costId;
  PlatformFile? pickedFile;
  String url;

  MessageBox(this.eventId, this.costId, this.pickedFile, this.url);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.grey.shade300,
        child: Builder(
            builder: (context) {
              Map<String, dynamic> message = {};
              return Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child:  TextFormField(
                        decoration: InputDecoration(
                          fillColor: Colors.yellow,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          contentPadding: const EdgeInsets.all(12),
                          hintText: "Type your message here...",
                        ),
                        onChanged: (text) {
                          message = Message(
                            text: text,
                            date: DateTime.now(),
                            author: user!.email!,
                          ).toJson();
                        },
                      ),
                    ),
                  ),
                  /*if(url != "asd")
                    Expanded(
                      child: Container(
                        color: Colors.blue,
                        child: Image.file(
                          File(url),
                          height: 200,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  CircleAvatar(
                    radius: 80,
                    backgroundImage: FileImage(File(url)),
                  ),*/
                  IconButton(
                    icon: const Icon(Icons.add_a_photo),
                    onPressed: () async {
                      context.read<ChatBloc>().add(PickedFileEvent());
                      if(url != "asd") {
                        var result = await showDialog(context: context, builder: (BuildContext context) => ShowPictureDialog(url));
                        if(result == true) {
                          var imageUrl = await injector<Interactor>().uploadPicture(pickedFile!.path!, pickedFile!.name);
                          message = Message(
                            text: "",
                            date: DateTime.now(),
                            author: user!.email!,
                            imageUrl: imageUrl,
                          ).toJson();
                          context.read<ChatBloc>().add(UploadMessage(eventId, costId, message));
                        }
                      }
                    },
                  ),
                  IconButton(
                    icon: const Icon(Icons.send),
                    onPressed: () {
                      if(message != {}) {
                        context.read<ChatBloc>().add(UploadMessage(eventId, costId, message));
                      }
                    },
                  ),
                ],
              );
            }
        )
    );
  }

}