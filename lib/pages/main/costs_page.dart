import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:costaccountant_app/bloc/costs/costs_bloc.dart';
import 'package:costaccountant_app/dialogs/create_cost_dialog.dart';
import 'package:costaccountant_app/domain/model/cost.dart';
import 'package:costaccountant_app/items/cost_item.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../di/di_utils.dart';
import '../drawer/navigation_drawer.dart';

class CostsPage extends StatelessWidget {

  final user = FirebaseAuth.instance.currentUser;
  late var _dataStream;
  late var _profiles;

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    final eventId = arguments["id"];
    var body = arguments["body"];
    return BlocProvider(
      create: (_) => injector<CostsBloc>()..add(InitialCosts(eventId)),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Costs"),
        ),
        body: BlocBuilder<CostsBloc, CostsState>(
          builder: (context, state) {
            print(state);
            if(state is InitialState) {
              return const Center(
                  child: CircularProgressIndicator(),
              );
            }
            else if (state is Changed) {
              print("tiszteletem");
              body = state.cost;
              context.read<CostsBloc>().add(Waiting());
            }
            else if (state is LoadedState) {
              _dataStream = state.costs;
              _profiles = state.profiles;
            }
            return Column(
                  children: [
                     SizedBox(
                      height: 100,
                        child: Center(
                          child: Text(
                            body,
                            style: const TextStyle(
                              fontSize: 40,
                            ),
                          ),
                        )
                    ),
                    Expanded(
                      child: StreamBuilder<QuerySnapshot>(
                        stream: _dataStream,
                        builder: (context, snapshot) {
                          if (snapshot.hasError || !snapshot.hasData) {
                            return const Center(child: Text("Something went wrong"));
                          }

                          if (snapshot.connectionState == ConnectionState.waiting) {
                            return const Center(child: CircularProgressIndicator());
                          }

                          var docs = snapshot.data!.docs;

                          if (docs.isEmpty) {
                            return const Center(
                              child: Text(
                                "Nothing",
                                textAlign: TextAlign.center,
                              ),
                            );
                          }
                          return ListView.builder(
                            itemCount: docs.length,
                            itemBuilder: (context, index) {
                              var doc = docs[index];
                              var cost = Cost.fromJson(doc.data());
                              return CostItem(cost, doc.id, eventId, _profiles);
                            },
                          );
                        },
                      ),
                    ),
                  ],
                );
          },
        ),
        drawer: NavigationDrawer(user!),
        floatingActionButton:
          Builder(
            builder: (context) {
              return FloatingActionButton(
                  onPressed: () async {
                    Cost result = await showDialog(context: context, builder: (BuildContext context) => CreateCostDialog());
                    context.read<CostsBloc>().add(UploadCost(eventId, result));
                  },
                  child: const Icon(Icons.android),
                );
            }
          ),
      ),
    );
  }
}