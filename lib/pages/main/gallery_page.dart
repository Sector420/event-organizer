import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class GalleryPage extends StatelessWidget {
  final PageController pageController;
  final List<String> urllist;
  final int index;
  var valami = [];

  GalleryPage({required this.urllist, required this.index}) : pageController = PageController(initialPage: index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PhotoViewGallery.builder(
          pageController: pageController,
          itemCount: urllist.length,
          builder: (context, index) {
            final urlImage = urllist[index];

            return PhotoViewGalleryPageOptions(
              imageProvider: NetworkImage(urlImage),
              minScale: PhotoViewComputedScale.contained,
              maxScale: PhotoViewComputedScale.contained * 4,
            );
          },
      ),
    );
  }

}