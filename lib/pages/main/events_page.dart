import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:costaccountant_app/domain/model/event.dart';
import 'package:costaccountant_app/dialogs/create_event_dialog.dart';

import 'package:costaccountant_app/items/event_item.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/events/events_bloc.dart';
import '../../di/di_utils.dart';
import '../drawer/navigation_drawer.dart';


class EventsPage extends StatelessWidget {
  final user = FirebaseAuth.instance.currentUser;

  late var _dataStream;
  late var _profiles;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => injector<EventsBloc>()..add(LoadProfileEvent(user!)),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Cost Accountant"),
        ),
        body: Column(
              children: [
                Builder(
                  builder: (context) {
                    return Container(
                      padding: const EdgeInsets.all(16),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          labelText: "Search term",
                          enabledBorder: UnderlineInputBorder(),
                        ),
                        onChanged: (text) {
                          context.read<EventsBloc>().add(UpdateSearchEvent(text));
                        },
                      ),
                    );
                  }
                ),
                Expanded(
                  child: BlocBuilder<EventsBloc, EventsState>(
                    builder: (context, state) {
                      if(state is StartingState) {
                        _dataStream = state.events;
                      }
                      if (state is InitialState) {
                        _dataStream = state.events;
                        _profiles = state.profiles;
                      }
                      else if (state is SearchResultState) {
                        _dataStream = state.events;
                      }
                      return StreamBuilder<QuerySnapshot>(
                        stream: _dataStream,
                        builder: (context, snapshot) {
                          if (snapshot.hasError || !snapshot.hasData) {
                            return const Center(child: Text("Something went wrong"));
                          }

                          if (snapshot.connectionState == ConnectionState.waiting) {
                            return const Center(child: CircularProgressIndicator());
                          }

                          var docs = snapshot.data!.docs;

                          if (docs.isEmpty) {
                            return const Center(
                              child: Text(
                                "Nothing",
                                textAlign: TextAlign.center,
                              ),
                            );
                          }
                          return ListView.builder(
                            itemCount: docs.length,
                            itemBuilder: (context, index) {
                              var doc = docs[index];
                              var event = Event.fromJson(doc.data());
                              return EventItem(event, doc.id, _profiles);
                            },
                          );
                        }
                      );
                    }
                  ),
                ),
              ],
            ),
        drawer: NavigationDrawer(user!),
        floatingActionButton: Builder(
          builder: (context) {
            return FloatingActionButton(
              onPressed: () async {
                var result = await showDialog(context: context, builder: (BuildContext context) => CreateEventDialog());
                context.read<EventsBloc>().add(UploadEvent(result));
              },
              child: const Icon(Icons.android),
            );
          }
        ),
      ),
    );
  }
}

