

import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:costaccountant_app/bloc/chat/chat_bloc.dart';
import 'package:costaccountant_app/domain/interactor/interactor.dart';
import 'package:costaccountant_app/items/message_item.dart';
import 'package:costaccountant_app/pages/main/message_box.dart';
import 'package:costaccountant_app/pages/main/pictures_page.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';

import '../../di/di_utils.dart';
import '../../domain/model/message.dart';

class CostChat extends StatelessWidget {
  late var _dataStream;
  final user = FirebaseAuth.instance.currentUser;
  PlatformFile? pickedFile;
  var url = "asd";
  List<String> urlList = [];

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    final eventId = arguments["eventId"];
    final costId = arguments["costId"];
    //injector<Interactor>().getProfileByEmail(user.email!);
    return BlocProvider(
      create: (context) => injector<ChatBloc>()..add(InitialChat(eventId, costId)),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Chat"),
          actions: [
            InkWell(
                child: const Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: Icon(Icons.photo_library)
                ),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder:(_) => PicturesPage(urlList)
                  ));
                  /*Navigator.pushNamed(
                    context,
                    "/pictures",
                    arguments: {"messages": urlList},
                  );*/
                },
            ),
          ],
        ),
        body: BlocBuilder<ChatBloc, ChatState>(
          builder: (context, state) {
            if(state is InitialState) {
              return const SizedBox(
                height: 100,
                width: 100,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            if(state is FileSelected) {
              pickedFile = state.result!.files.first;
              url = pickedFile!.path!;
            }
            else if (state is LoadedState) {
              _dataStream = state.messages;
            }
            return Column(
              children: [
                Expanded(
                    child: StreamBuilder<QuerySnapshot>(
                      stream: _dataStream,
                      builder: (context, snapshot) {
                        if (snapshot.hasError || !snapshot.hasData) {
                          return const Center(child: Text("Something went wrong"));
                        }
                        var docs = snapshot.data!.docs;
                        List<Message> items = List.from(
                          snapshot.data!.docs
                              .map((post) => Message.fromJson(post.data() as Map<String, dynamic>))
                              .toList(),
                        );
                        return GroupedListView<Message, DateTime>(
                            reverse: true,
                            elements: items,
                            order: GroupedListOrder.DESC,
                            useStickyGroupSeparators: true,
                            floatingHeader: true,
                            groupBy: (message) => DateTime(
                              message.date.year,
                              message.date.month,
                              message.date.day,
                            ),
                            groupHeaderBuilder: (Message message) => SizedBox(
                              height: 40,
                              child: Center(
                                child: Card(
                                    color: Theme.of(context).primaryColor,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        DateFormat.yMMMd().format(message.date),
                                        style: const TextStyle(color: Colors.white),
                                      )
                                    )
                                ),
                              ),
                            ),
                            indexedItemBuilder: (context, Message message, index) {
                              var doc = docs[index];
                              var message = Message.fromJson(doc.data());
                              if(message.imageUrl != null && !urlList.contains(message.imageUrl)) {
                                urlList.add(message.imageUrl!);
                              }
                              return MessageItem(message);
                            },
                        );
                      }
                    ),
                ),
                MessageBox(eventId, costId, pickedFile, url),
              ],
            );
          },
        )
      ),
    );
  }

}