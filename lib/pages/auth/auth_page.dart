
import 'package:costaccountant_app/pages/auth/signup_page.dart';
import 'package:flutter/cupertino.dart';
import 'login_page.dart';

class AuthPage extends StatefulWidget {

  @override
  _AuthPageState createState() => _AuthPageState();

}

class _AuthPageState extends State<AuthPage> {
  bool isLogin = true;

  @override
  Widget build(BuildContext context) =>
    isLogin ? LoginPage(onClickedSignUp: change,) : SignupPage(onClickedSignIn: change);
  //isLogin ? LoginPage() : SignupPage();


  void change() => setState(() => isLogin = !isLogin);

}