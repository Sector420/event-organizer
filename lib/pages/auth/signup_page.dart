import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../domain/model/profile.dart';

class SignupPage extends StatefulWidget {
  final VoidCallback onClickedSignIn;

  const SignupPage({
    Key?  key,
    required this.onClickedSignIn,
  }) : super (key: key);

  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmController = TextEditingController();
  final profiles = FirebaseFirestore.instance.collection("profiles");

  Future<void> _tryRegistration(BuildContext context) async {
    final email = _emailController.text;
    final password = _passwordController.text;

    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      // adj alapértelmezett imageurlt is
      final newProfile = Profile(
        username : "BÉLA",
        email : email,
      ).toJson();

      profiles.add(newProfile);


      print("User registration successful! Logging in...");

      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);


      Navigator.pushReplacementNamed(context, "/posts");
    } on FirebaseAuthException catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(e.message!)));
    } on Exception catch (e) {
      print("User registration/login failed: ${e.toString()}");
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Registration failed, please try again!")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Cost Accountant"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.android,
                      size: 120,
                    ),
                  ],
                ),
                TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                    alignLabelWithHint: true,
                    labelText: "Email address",
                  ),
                  keyboardType: TextInputType.emailAddress,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (email) => email != null && !EmailValidator.validate(email)
                      ? "Enter a valid email"
                      : null,
                ),
                TextFormField(
                  controller: _passwordController,
                  decoration: const InputDecoration(
                    alignLabelWithHint: true,
                    labelText: "Password",
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: true,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) => value != null && value.length < 6
                    ? "Enter min. 6 characters"
                    : null,
                ),
                TextFormField(
                  controller: _confirmController,
                  decoration: const InputDecoration(
                    alignLabelWithHint: true,
                    labelText: "Confirm Password",
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: true,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) => _confirmController.text != _passwordController.text
                      ? "Confirm your password"
                      : null,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () {
                            _tryRegistration(context);
                          },
                          child: Text("Register".toUpperCase()),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: RichText(
                      text: TextSpan(
                          style: const TextStyle(color: Colors.black),
                          text: "Already have an account? ",
                          children: [
                            TextSpan(
                                recognizer: TapGestureRecognizer()
                                  /*..onTap = () {
                                    Navigator.pushNamed(
                                      context,
                                      "/login",
                                    );
                                  },*/
                                  ..onTap = widget.onClickedSignIn,
                                text: "Log In",
                                style: const TextStyle(
                                  decoration: TextDecoration.underline,
                                )
                            )
                          ]
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
