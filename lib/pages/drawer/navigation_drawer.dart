import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../di/di_utils.dart';
import '../../dialogs/join_dialog.dart';
import '../../domain/interactor/interactor.dart';
import '../../domain/model/profile.dart';
import '../auth/auth_page.dart';

class NavigationDrawer extends StatelessWidget {
  final User user;

  NavigationDrawer(this.user);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            buildHeader(context),
            buildMenuItems(context),
          ],
        ),
      ),
    );
  }

  buildHeader(BuildContext context) {
    return FutureBuilder<Profile?>(
      future: injector<Interactor>().getProfileByEmail(user.email!),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var profile = snapshot.data;
          return Container(
              color: Colors.blue.shade600,
              padding: const EdgeInsets.only(top: 50, bottom: 10),
              child: Column (
                children: [
                  InkWell(
                      child: CircleAvatar(
                          radius: 52,
                          backgroundImage: NetworkImage(profile!.imageUrl!)
                      ),
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          "/profilepage",
                        );
                      }
                      ),
                  const SizedBox(height: 15),
                  Text(
                      user.email!.substring(0, user.email!.indexOf("@")),
                      style: const TextStyle(fontSize: 30, color: Colors.white)
                  ),
                  const SizedBox(height: 15),
                  Text(
                      user.email!,
                      style: const TextStyle(fontSize: 15, color: Colors.white)
                  ),
                ],
              )
          );
        }
        else {
          return const CircularProgressIndicator();
        }
        },
    );
  }

  buildMenuItems(BuildContext context) {
    return Wrap (
      runSpacing: 16,
      children: [
        const ListTile(
            leading: Icon(Icons.home),
            title: Text("Home")
        ),
        ListTile(
          leading: const Icon(Icons.arrow_forward),
          title: const Text("Join to event"),
          onTap: () {
            showDialog(context: context, builder: (BuildContext context) => JoinDialog());
          },
        ),
        ListTile(
          leading: const Icon(Icons.logout),
          title: const Text("Log out"),
          onTap: () {
            _logOut();
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => AuthPage(),
              ),
            );
          },
        ),
      ],
    );
  }

  Future<void> _logOut() {
    return FirebaseAuth.instance.signOut();
  }

}
