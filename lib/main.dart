import 'package:costaccountant_app/pages/auth/auth_page.dart';
import 'package:costaccountant_app/pages/main/cost_chat.dart';
import 'package:costaccountant_app/pages/main/pictures_page.dart';
import 'package:costaccountant_app/pages/main/costs_page.dart';
import 'package:costaccountant_app/pages/auth/forgot_password_page.dart';
import 'package:costaccountant_app/pages/main/events_page.dart';
import 'package:costaccountant_app/pages/main/profile_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';

import 'package:flutter/foundation.dart' show kIsWeb;

import 'di/di_utils.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initFirebase();
  initDependencies();
  runApp(FirebaseInitializer());
}

Future<FirebaseApp> initFirebase() async {
  final firebaseApp = await Firebase.initializeApp();
  if (kDebugMode && !kIsWeb) {
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  }
  return firebaseApp;
}

class FirebaseInitializer extends StatefulWidget {
  @override
  _FirebaseInitializerState createState() => _FirebaseInitializerState();
}

class _FirebaseInitializerState extends State<FirebaseInitializer> {

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: injector.allReady(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return CostAccountantApp();
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
    );
  }
}

class CostAccountantApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cost Accountant',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreenRenderer(),
      routes: {
        "/login": (context) => AuthPage(),
        //"/signup": (context) => SignupPage(),
        "/forgot": (context) => ForgotPasswordPage(),
        "/posts": (context) => EventsPage(),
        "/profilepage": (context) => ProfilePage(),
        "/costspage": (context) => CostsPage(),
        //"/pictures": (context) => PicturesPage(),
        "/costchat": (context) => CostChat(),
      },
    );
  }
}

class HomeScreenRenderer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Conditional.single(
      context: context,
      conditionBuilder: (context) {
        return FirebaseAuth.instance.currentUser != null;
      },
      widgetBuilder: (context) => EventsPage(),
      fallbackBuilder: (context) => AuthPage(),
    );
  }
}
