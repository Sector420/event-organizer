import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

import 'fb_api.dart';

class FBService implements FBApi {
  final user = FirebaseAuth.instance.currentUser;

  @override
  CollectionReference<Map<String, dynamic>> getEvents() {
    return FirebaseFirestore.instance.collection("events");
  }

  @override
  CollectionReference<Map<String, dynamic>> getCosts(String eventId) {
    return FirebaseFirestore.instance.collection("events").doc(eventId).collection("costs");
  }

  @override
  CollectionReference<Map<String, dynamic>> getMessages(String eventId, String costId) {
    return FirebaseFirestore.instance.collection("events").doc(eventId).collection("costs").doc(costId).collection("messages");
  }

  @override
  CollectionReference<Map<String, dynamic>> getProfiles() {
    return FirebaseFirestore.instance.collection("profiles");
  }

  @override
  FirebaseStorage getStorage() {
    return FirebaseStorage.instance;
  }

}