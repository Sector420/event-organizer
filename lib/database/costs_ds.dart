import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:costaccountant_app/domain/model/cost.dart';

import 'fb_api.dart';

class CostsDataSource {
  final FBApi _fbApi;

  CostsDataSource(this._fbApi);

  Stream<QuerySnapshot<Map<String, dynamic>>> getCosts(String eventId) async* {
    yield* _fbApi.getCosts(eventId).snapshots();
  }

  void addCost(String eventId, Cost newCost) {
    _fbApi.getCosts(eventId).add(newCost.toJson());
  }

  void deleteCost(String eventId, String costId) {
    _fbApi.getCosts(eventId).doc(costId).delete();
  }

}