import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';

import '../domain/model/profile.dart';
import 'fb_api.dart';

class ProfilesDataSource {
  final FBApi _fbApi;

  ProfilesDataSource(this._fbApi);

  Future<List<Profile>> getProfiles() async {
    var querySnapshot = await _fbApi.getProfiles().get();
    final profiles = querySnapshot.docs.map((doc) => Profile.fromJson(doc.data())).toList();
    return profiles;
  }

  Future<String> getIdByEmail(String email) async {
    String? id;
    await _fbApi.getProfiles().snapshots().map((s) => s.docs.map((doc) => id = doc.id).toList()).first;
    return id!;
  }

  Future<Profile> getProfileByEmail(String email) async {
    var querySnapshot = await _fbApi.getProfiles().where("email", isEqualTo: email).get();
    final profile = querySnapshot.docs.map((doc) => Profile.fromJson(doc.data())).toList().first;
    return profile;
  }

  Future<void> uploadFile (String filePath, String fileName, String id)  async {
    File file = File(filePath);
    try {
      _fbApi.getStorage().ref('test/$fileName').putFile(file);
      final path = "test/${fileName}";
      final imageURl = await _fbApi.getStorage().ref().child(path).getDownloadURL();
      _fbApi.getProfiles().doc(id).update({"imageUrl" : imageURl});
    } on FirebaseException catch(e) {
      print(e);
    }
  }

}