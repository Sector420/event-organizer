import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'fb_api.dart';

class MessagesDataSource {
  final FBApi _fbApi;

  MessagesDataSource(this._fbApi);

  Stream<QuerySnapshot<Map<String, dynamic>>> getMessages(String eventId, String costId) async* {
    yield* _fbApi.getMessages(eventId, costId).orderBy("date", descending: true).snapshots();
  }

  void addMessage(String eventId, String costId, Map <String, dynamic> newMessage) {
    _fbApi.getMessages(eventId, costId).add(newMessage);
  }

  Future<String> uploadPicture (String filePath, String fileName)  async {
    File file = File(filePath);
    _fbApi.getStorage().ref('messages/$fileName').putFile(file);
    final path = "test/${fileName}";
    final imageURl = await _fbApi.getStorage().ref().child(path).getDownloadURL();
    return imageURl;
  }

}