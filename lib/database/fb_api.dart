import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

abstract class FBApi {
  CollectionReference<Map<String, dynamic>> getEvents();
  CollectionReference<Map<String, dynamic>> getCosts(String eventId);
  CollectionReference<Map<String, dynamic>> getMessages(String eventId, String costId);
  CollectionReference<Map<String, dynamic>> getProfiles();
  FirebaseStorage getStorage();

}