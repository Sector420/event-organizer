import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../domain/model/event.dart';
import 'fb_api.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class EventsDataSource {
  final user = FirebaseAuth.instance.currentUser;
  final FBApi _fbApi;

  EventsDataSource(this._fbApi);

  Stream<QuerySnapshot<Map<String, dynamic>>> kapdbeafaszom() async* {
    print("kezdetek");
  }


  //ezeket mondjuk mappalni
  Stream<QuerySnapshot<Map<String, dynamic>>> getEvents(String email) async* {
    final events = _fbApi.getEvents();
    yield* events.where("theboys", arrayContains: email)
        .orderBy("time", descending: true)
        .snapshots();
  }

  Future<Event> getEvent(String id) async {
    final docEvent = _fbApi.getEvents().doc(id);
    final snapshot = await docEvent.get();

    return Event.fromJson(snapshot.data());
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getSearchResult(String filter) async* {
    final events = _fbApi.getEvents();
    yield* events.where("theboys", arrayContains: user?.email).where("title", isEqualTo: filter)
        .orderBy("time", descending: true)
        .snapshots();
  }

  void addEvent(Map <String, dynamic> newEvent) {
     _fbApi.getEvents().add(newEvent);
  }

  void deleteEvent(String eventId) {
    _fbApi.getEvents().doc(eventId).delete();
  }

  void updateEvent(List<String?> members, String eventId) {
    _fbApi.getEvents().doc(eventId).update({"theboys": members});
  }

  void addCostToEvent(String cost, String eventId) {
    _fbApi.getEvents().doc(eventId).update({"body": cost});
  }
}