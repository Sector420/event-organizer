import 'package:json_annotation/json_annotation.dart';

part 'event.g.dart';

@JsonSerializable()
class Event {
  final String author;
  final String title;
  final DateTime time;
  final String? body;
  final List<String?> theboys;

  Event({
    required this.author,
    required this.title,
    required this.time,
    this.body,
    required this.theboys,
  });

  factory Event.fromJson(dynamic json) => _$EventFromJson(json);

  Map<String, dynamic> toJson() => _$EventToJson(this);
}