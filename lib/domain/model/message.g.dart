// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) {
  return Message(
    text: json['text'] as String,
    date: DateTime.parse(json['date'] as String),
    author: json['author'] as String,
    imageUrl: json['imageUrl'] as String?,
  );
}

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'text': instance.text,
      'date': instance.date.toIso8601String(),
      'author': instance.author,
      'imageUrl': instance.imageUrl,
    };
