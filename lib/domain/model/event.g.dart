// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Event _$EventFromJson(Map<String, dynamic> json) {
  return Event(
    author: json['author'] as String,
    title: json['title'] as String,
    time: DateTime.parse(json['time'] as String),
    body: json['body'] as String?,
    theboys:
        (json['theboys'] as List<dynamic>).map((e) => e as String?).toList(),
  );
}

Map<String, dynamic> _$EventToJson(Event instance) => <String, dynamic>{
      'author': instance.author,
      'title': instance.title,
      'time': instance.time.toIso8601String(),
      'body': instance.body,
      'theboys': instance.theboys,
    };
