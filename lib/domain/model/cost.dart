import 'package:json_annotation/json_annotation.dart';

part 'cost.g.dart';

@JsonSerializable()
class Cost {
  final String title;
  final String body;
  final String author;

  Cost({
    required this.title,
    required this.body,
    required this.author,
  });

  factory Cost.fromJson(dynamic json) => _$CostFromJson(json);

  Map<String, dynamic> toJson() => _$CostToJson(this);
}