// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cost.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cost _$CostFromJson(Map<String, dynamic> json) {
  return Cost(
    title: json['title'] as String,
    body: json['body'] as String,
    author: json['author'] as String,
  );
}

Map<String, dynamic> _$CostToJson(Cost instance) => <String, dynamic>{
      'title': instance.title,
      'body': instance.body,
      'author': instance.author,
    };
