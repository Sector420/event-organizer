import 'package:json_annotation/json_annotation.dart';

part 'profile.g.dart';


@JsonSerializable()
class Profile {
  final String username;
  final String email;
  final String? imageUrl;

  Profile({
    required this.username,
    required this.email,
    this.imageUrl,
  });

  factory Profile.fromJson(dynamic json) => _$ProfileFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileToJson(this);
}