import 'package:json_annotation/json_annotation.dart';

part 'message.g.dart';

@JsonSerializable()
class Message {
  final String text;
  final DateTime date;
  final String author;
  final String? imageUrl;

  Message({
    required this.text,
    required this.date,
    required this.author,
    this.imageUrl,
  });

  factory Message.fromJson(dynamic json) => _$MessageFromJson(json);

  Map<String, dynamic> toJson() => _$MessageToJson(this);
}