import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:costaccountant_app/database/messages_ds.dart';
import 'package:costaccountant_app/domain/model/cost.dart';

import '../../database/costs_ds.dart';
import '../../database/events_ds.dart';
import '../../database/profiles_ds.dart';
import '../model/event.dart';
import '../model/profile.dart';

class Interactor {
  final EventsDataSource _eventsDataSource;
  final CostsDataSource _costsDataSource;
  final MessagesDataSource _messagesDataSource;
  final ProfilesDataSource _profilesDataSource;

  Interactor(this._eventsDataSource, this._costsDataSource, this._messagesDataSource, this._profilesDataSource);


  Stream<QuerySnapshot<Map<String, dynamic>>> kapdbeafaszom() async* {
    yield* _eventsDataSource.kapdbeafaszom();
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getEvents(String email) async* {
    yield* _eventsDataSource.getEvents(email);
  }

  Future<Event> getEventById (String id) async {
    return await _eventsDataSource.getEvent(id);
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getSearchResult(String filter) async* {
    yield* _eventsDataSource.getSearchResult(filter);
  }

  void addEvent(Map <String, dynamic> newEvent) {
    _eventsDataSource.addEvent(newEvent);
  }

  void updateEvent(List<String?> members, String eventId) {
    _eventsDataSource.updateEvent(members, eventId);
  }

  void addCostToEvent(String cost, String eventId) {
    _eventsDataSource.addCostToEvent(cost, eventId);
  }

  void deleteEvent(String eventId) {
    _eventsDataSource.deleteEvent(eventId);
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getCosts(String eventId) async* {
    yield* _costsDataSource.getCosts(eventId);
  }

  void addCost(String eventId, Cost newCost) {
    _costsDataSource.addCost(eventId, newCost);
  }

  void deleteCost(String eventId, String costId) {
    _costsDataSource.deleteCost(eventId, costId);
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getMessages(String eventId, String costId) async* {
    yield* _messagesDataSource.getMessages(eventId, costId);
  }

  void addMessage(String eventId, String costId, Map <String, dynamic> newMessage) {
    _messagesDataSource.addMessage(eventId, costId, newMessage);
  }

  Future<String> uploadPicture (String filePath, String fileName)  async {
    return await _messagesDataSource.uploadPicture(filePath, fileName);
  }

  Future<List<Profile>> getProfiles() async {
    return await _profilesDataSource.getProfiles();
  }

  Future<String> getIdByEmail(String email) async {
    return await _profilesDataSource.getIdByEmail(email);
  }

  Future<Profile?> getProfileByEmail(String email) async {
    return await _profilesDataSource.getProfileByEmail(email);
  }

  Future<void> uploadFile (String filePath, String fileName, String id)  async {
    return await _profilesDataSource.uploadFile(filePath, fileName, id);
  }

}