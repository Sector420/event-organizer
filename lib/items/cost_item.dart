import 'package:costaccountant_app/domain/model/cost.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

import '../bloc/costs/costs_bloc.dart';
import '../domain/model/profile.dart';


class CostItem extends StatelessWidget {
  final Cost cost;
  final String costId;
  final String eventId;
  final List<Profile> profiles;

  CostItem(this.cost, this.costId, this.eventId, this.profiles);

  @override
  Widget build(BuildContext context) {
    String? url;
    for (var element in profiles) {
      if (element.email == cost.author) {
        url = element.imageUrl;
      }
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: [
          Expanded(
            child: Card(
              color: Colors.amberAccent,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                      child: CircleAvatar(
                        radius: 30,
                        backgroundImage: NetworkImage(url!),
                      ),
                  ),
                  Expanded(
                    child: ListTile(
                      title: Text(cost.title),
                      subtitle: Text(
                        cost.body,
                      ),
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          "/costchat",
                          arguments: {"eventId" : eventId, "costId" : costId},
                        );
                      },
                    ),
                  ),
                  IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () {
                        context.read<CostsBloc>().add(DeleteCost(eventId, costId, cost));
                      }
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
