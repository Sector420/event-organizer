import 'package:costaccountant_app/domain/model/event.dart';
import 'package:costaccountant_app/domain/model/profile.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import '../bloc/events/events_bloc.dart';


class EventItem extends StatelessWidget {
  final Event event;
  final String id;
  final List<Profile> profiles;

  EventItem(this.event, this.id, this.profiles);

  @override
  Widget build(BuildContext context) {
    String? url;
    for (var element in profiles) {
      if (element.email == event.author) {
        url = element.imageUrl;
      }
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: [
          Expanded(
            child: Card(
              color: Colors.lightBlueAccent,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: CircleAvatar(
                      radius: 30,
                      backgroundImage: NetworkImage(url!),
                    ),
                  ),
                  Expanded(
                    child: ListTile(
                      title: Text(event.title),
                      subtitle: Text(event.body!,),
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          "/costspage",
                          arguments: {"id": id , "body": event.body},
                        );
                      },
                    ),
                  ),
                  IconButton(
                      icon: const Icon(Icons.share),
                      onPressed: () {
                        _sendEmail("Share event", id,);
                      }
                  ),
                  IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () {
                        context.read<EventsBloc>().add(DeleteEvent(id));
                      }
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _sendEmail(String subject, String body) {
    final url = "mailto:?subject=${Uri.encodeFull(subject)}&body=${Uri.encodeFull(body)}";
    launch(url);
  }
}
