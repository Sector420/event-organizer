import 'package:costaccountant_app/pages/main/gallery_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PhotoItem extends StatelessWidget {
  final int index;
  final List<String> urllist;

  PhotoItem(this.index, this.urllist);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Image.network(
          urllist[index],
          fit: BoxFit.cover,
      ),
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder:(_) => GalleryPage(urllist: urllist, index: index)
        ));
      },
    );
  }
}