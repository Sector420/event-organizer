import 'package:costaccountant_app/domain/model/event.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';


class CreateEventDialog extends StatefulWidget {

  @override
  _CreateEventDialogState createState() => _CreateEventDialogState();
}

class _CreateEventDialogState extends State<CreateEventDialog> {
  final _eventTitleController = TextEditingController();
  final _eventBodyController = TextEditingController();
  final user = FirebaseAuth.instance.currentUser;
  bool _validate = false;

  Map<String, dynamic> _createEvent(BuildContext context) {
    List<String?> list = [];
    list.add(user!.email);
    final newEvent = Event(
      author: FirebaseAuth.instance.currentUser?.email ?? "Anonymous",
      title: _eventTitleController.text,
      time: DateTime.now(),
      body: _eventBodyController.text,
      theboys: list,
    ).toJson();

    return newEvent;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      backgroundColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _eventTitleController,
              decoration: InputDecoration(
                alignLabelWithHint: true,
                labelText: "Title",
                errorText: _validate ? 'Value Can\'t Be Empty' : null,
              ),
            ),
            TextField(
              controller: _eventBodyController,
              decoration: const InputDecoration(
                alignLabelWithHint: true,
                labelText: "Write information about your event",
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            ElevatedButton(
              onPressed: () async {
                setState(() {
                  _eventTitleController.text.isEmpty ? _validate = true : _validate = false;
                });
                if(!_validate) {
                  final newEvent = _createEvent(context);
                  Navigator.pop(context, newEvent);
                }
              },
              child: Text("Post".toUpperCase()),
            ),
          ],
        ),
      ),
    );
  }
}
