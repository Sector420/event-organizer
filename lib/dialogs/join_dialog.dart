import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../di/di_utils.dart';
import '../domain/interactor/interactor.dart';

class JoinDialog extends StatelessWidget {

  final _eventIdController = TextEditingController();
  final user = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      backgroundColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _eventIdController,
              decoration: const InputDecoration(
                alignLabelWithHint: true,
                labelText: "Id",
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: ()  {
                    Navigator.pop(context);
                  },
                  child: Text("Cancel".toUpperCase()),
                ),
                ElevatedButton(
                  onPressed: ()  {
                    _join(_eventIdController.text);
                    Navigator.pop(context);
                  },
                  child: Text("Join".toUpperCase()),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _join (String id) async {
    var event = await injector<Interactor>().getEventById(id);
    event.theboys.add(user?.email);
    injector<Interactor>().updateEvent(event.theboys, id);
  }
}