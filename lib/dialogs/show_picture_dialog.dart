import 'dart:io';

import 'package:flutter/material.dart';

class ShowPictureDialog extends StatelessWidget {
  String url;

  ShowPictureDialog(this.url);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      backgroundColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.file(
              File(url),
              height: 200,
              fit: BoxFit.cover,
            ),
            const SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: ()  {
                    Navigator.pop(context);
                  },
                  child: Text("Cancel".toUpperCase()),
                ),
                ElevatedButton(
                  onPressed: ()  {
                    Navigator.pop(context, true);
                  },
                  child: Text("Send".toUpperCase()),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}