import 'package:costaccountant_app/domain/model/cost.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class CreateCostDialog extends StatelessWidget {

  final _eventTitleController = TextEditingController();
  final _eventBodyController = TextEditingController();
  final user = FirebaseAuth.instance.currentUser;

  Cost _createCost(BuildContext context) {
    final newCost = Cost(
      title: _eventTitleController.text,
      body: _eventBodyController.text,
      author: user!.email!,
    );
    return newCost;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 0,
      backgroundColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _eventTitleController,
              decoration: const InputDecoration(
                alignLabelWithHint: true,
                labelText: "Title",
              ),
            ),
            TextField(
              controller: _eventBodyController,
              decoration: const InputDecoration(
                alignLabelWithHint: true,
                labelText: "Write information about your cost",
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            ElevatedButton(
              onPressed: () async {
                final newCost = _createCost(context);
                Navigator.pop(context, newCost);
              },
              child: Text("Post".toUpperCase()),
            ),
          ],
        ),
      ),
    );
  }
}